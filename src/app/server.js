'use strict';

const express = require('express');
const app = express();
const host = '0.0.0.0';
const port = process.env.PORT || 8080;

function getPrettyJson(value) {
  return JSON.stringify(value, null, 2);
}

function getAllEndpoints(request, response) {
  var endpoints = [];
  app._router.stack.forEach(function (endpoint) {
    if (endpoint.route && endpoint.route.path) {
      endpoints.push({ 'endpoint': endpoint.route.path });
    }
  });

  var endpointsJson = getPrettyJson({ 'endpoints': endpoints });
  response.send(endpointsJson);
};

function getEcho(request, response) {
  var hello = getPrettyJson({ 'value': 'Hello from server in container!' });
  response.send(hello);
};

function getDateTime(request, response) {
  var currentDate = getPrettyJson({ 'value': new Date().toLocaleString('pl-PL') });
  response.send(currentDate);
};

app.get('/', (request, response) => getAllEndpoints(request, response));
app.get('/api', (request, response) => getAllEndpoints(request, response));
app.get('/api/endpoint', (request, response) => getAllEndpoints(request, response));
app.get('/api/echo', (request, response) => getEcho(request, response));
app.get('/api/datetime', (request, response) => getDateTime(request, response));

app.listen(port, host);

console.log(`> Running server on http://${host}:${port}`);